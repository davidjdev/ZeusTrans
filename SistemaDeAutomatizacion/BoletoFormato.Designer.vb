﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class BoletoFormato
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim CedulaLabel As System.Windows.Forms.Label
        Dim ApellidoLabel As System.Windows.Forms.Label
        Dim NombreLabel As System.Windows.Forms.Label
        Dim Label4 As System.Windows.Forms.Label
        Dim Label3 As System.Windows.Forms.Label
        Dim Label1 As System.Windows.Forms.Label
        Dim Label2 As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(BoletoFormato))
        Dim Label12 As System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.nombre = New System.Windows.Forms.Label()
        Me.apellido = New System.Windows.Forms.Label()
        Me.cedula = New System.Windows.Forms.Label()
        Me.fecha_actual = New System.Windows.Forms.Label()
        Me.hora_actual = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.imprimir = New Microsoft.VisualBasic.PowerPacks.Printing.PrintForm(Me.components)
        Me.ErrorProvider1 = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        CedulaLabel = New System.Windows.Forms.Label()
        ApellidoLabel = New System.Windows.Forms.Label()
        NombreLabel = New System.Windows.Forms.Label()
        Label4 = New System.Windows.Forms.Label()
        Label3 = New System.Windows.Forms.Label()
        Label1 = New System.Windows.Forms.Label()
        Label2 = New System.Windows.Forms.Label()
        Label12 = New System.Windows.Forms.Label()
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'CedulaLabel
        '
        CedulaLabel.AutoSize = True
        CedulaLabel.BackColor = System.Drawing.Color.Transparent
        CedulaLabel.Font = New System.Drawing.Font("Agency FB", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CedulaLabel.Location = New System.Drawing.Point(7, 57)
        CedulaLabel.Name = "CedulaLabel"
        CedulaLabel.Size = New System.Drawing.Size(70, 31)
        CedulaLabel.TabIndex = 28
        CedulaLabel.Text = "Cedula:"
        '
        'ApellidoLabel
        '
        ApellidoLabel.AutoSize = True
        ApellidoLabel.BackColor = System.Drawing.Color.Transparent
        ApellidoLabel.Font = New System.Drawing.Font("Agency FB", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ApellidoLabel.Location = New System.Drawing.Point(8, 111)
        ApellidoLabel.Name = "ApellidoLabel"
        ApellidoLabel.Size = New System.Drawing.Size(78, 31)
        ApellidoLabel.TabIndex = 29
        ApellidoLabel.Text = "Apellido:"
        '
        'NombreLabel
        '
        NombreLabel.AutoSize = True
        NombreLabel.BackColor = System.Drawing.Color.Transparent
        NombreLabel.Font = New System.Drawing.Font("Agency FB", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NombreLabel.Location = New System.Drawing.Point(10, 164)
        NombreLabel.Name = "NombreLabel"
        NombreLabel.Size = New System.Drawing.Size(79, 31)
        NombreLabel.TabIndex = 30
        NombreLabel.Text = "Nombre:"
        '
        'Label4
        '
        Label4.AutoSize = True
        Label4.BackColor = System.Drawing.Color.Transparent
        Label4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Label4.Font = New System.Drawing.Font("Agency FB", 18.0!, System.Drawing.FontStyle.Bold)
        Label4.Location = New System.Drawing.Point(130, 115)
        Label4.Name = "Label4"
        Label4.Size = New System.Drawing.Size(127, 31)
        Label4.TabIndex = 40
        Label4.Text = "Fecha de venta"
        '
        'Label3
        '
        Label3.AutoSize = True
        Label3.BackColor = System.Drawing.Color.Transparent
        Label3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Label3.Font = New System.Drawing.Font("Agency FB", 18.0!, System.Drawing.FontStyle.Bold)
        Label3.Location = New System.Drawing.Point(130, 56)
        Label3.Name = "Label3"
        Label3.Size = New System.Drawing.Size(121, 31)
        Label3.TabIndex = 39
        Label3.Text = "Hora de venta"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.BackColor = System.Drawing.Color.Transparent
        Me.Label9.Font = New System.Drawing.Font("Agency FB", 26.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(418, 128)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(120, 42)
        Me.Label9.TabIndex = 36
        Me.Label9.Text = "4500,00"
        '
        'Label1
        '
        Label1.AutoSize = True
        Label1.BackColor = System.Drawing.Color.Transparent
        Label1.Font = New System.Drawing.Font("Agency FB", 18.0!, System.Drawing.FontStyle.Bold)
        Label1.Location = New System.Drawing.Point(285, 57)
        Label1.Name = "Label1"
        Label1.Size = New System.Drawing.Size(147, 31)
        Label1.TabIndex = 42
        Label1.Text = "Codigo de unidad:"
        '
        'Label2
        '
        Label2.AutoSize = True
        Label2.BackColor = System.Drawing.Color.Transparent
        Label2.Font = New System.Drawing.Font("Agency FB", 18.0!, System.Drawing.FontStyle.Bold)
        Label2.Location = New System.Drawing.Point(343, 88)
        Label2.Name = "Label2"
        Label2.Size = New System.Drawing.Size(89, 31)
        Label2.TabIndex = 43
        Label2.Text = "Matricula:"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.BackColor = System.Drawing.Color.Transparent
        Me.Label7.Font = New System.Drawing.Font("Agency FB", 13.0!, System.Drawing.FontStyle.Bold)
        Me.Label7.Location = New System.Drawing.Point(131, 173)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(119, 22)
        Me.Label7.TabIndex = 45
        Me.Label7.Text = "DIRECCION OFICINA"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.Color.Transparent
        Me.Label6.Font = New System.Drawing.Font("Agency FB", 13.0!, System.Drawing.FontStyle.Bold)
        Me.Label6.Location = New System.Drawing.Point(131, 195)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(134, 22)
        Me.Label6.TabIndex = 44
        Me.Label6.Text = "NOMBRE DEL DESTINO"
        '
        'nombre
        '
        Me.nombre.AutoSize = True
        Me.nombre.BackColor = System.Drawing.Color.Transparent
        Me.nombre.Font = New System.Drawing.Font("Agency FB", 13.0!, System.Drawing.FontStyle.Bold)
        Me.nombre.Location = New System.Drawing.Point(12, 195)
        Me.nombre.Name = "nombre"
        Me.nombre.Size = New System.Drawing.Size(55, 22)
        Me.nombre.TabIndex = 34
        Me.nombre.Text = "Roberto"
        '
        'apellido
        '
        Me.apellido.AutoSize = True
        Me.apellido.BackColor = System.Drawing.Color.Transparent
        Me.apellido.Font = New System.Drawing.Font("Agency FB", 13.0!, System.Drawing.FontStyle.Bold)
        Me.apellido.Location = New System.Drawing.Point(11, 146)
        Me.apellido.Name = "apellido"
        Me.apellido.Size = New System.Drawing.Size(61, 22)
        Me.apellido.TabIndex = 33
        Me.apellido.Text = "Ledezma"
        '
        'cedula
        '
        Me.cedula.AutoSize = True
        Me.cedula.BackColor = System.Drawing.Color.Transparent
        Me.cedula.Font = New System.Drawing.Font("Agency FB", 13.0!, System.Drawing.FontStyle.Bold)
        Me.cedula.Location = New System.Drawing.Point(10, 88)
        Me.cedula.Name = "cedula"
        Me.cedula.Size = New System.Drawing.Size(61, 22)
        Me.cedula.TabIndex = 32
        Me.cedula.Text = "V241222"
        '
        'fecha_actual
        '
        Me.fecha_actual.AutoSize = True
        Me.fecha_actual.BackColor = System.Drawing.Color.Transparent
        Me.fecha_actual.Font = New System.Drawing.Font("Agency FB", 13.0!, System.Drawing.FontStyle.Bold)
        Me.fecha_actual.Location = New System.Drawing.Point(131, 146)
        Me.fecha_actual.Name = "fecha_actual"
        Me.fecha_actual.Size = New System.Drawing.Size(100, 22)
        Me.fecha_actual.TabIndex = 41
        Me.fecha_actual.Text = "#########"
        '
        'hora_actual
        '
        Me.hora_actual.AutoSize = True
        Me.hora_actual.BackColor = System.Drawing.Color.Transparent
        Me.hora_actual.Font = New System.Drawing.Font("Agency FB", 13.0!, System.Drawing.FontStyle.Bold)
        Me.hora_actual.Location = New System.Drawing.Point(132, 89)
        Me.hora_actual.Name = "hora_actual"
        Me.hora_actual.Size = New System.Drawing.Size(100, 22)
        Me.hora_actual.TabIndex = 38
        Me.hora_actual.Text = "#########"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.BackColor = System.Drawing.Color.Transparent
        Me.Label10.Font = New System.Drawing.Font("Agency FB", 26.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(367, 128)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(48, 42)
        Me.Label10.TabIndex = 37
        Me.Label10.Text = "Bs"
        '
        'imprimir
        '
        Me.imprimir.DocumentName = "document"
        Me.imprimir.Form = Me
        Me.imprimir.PrintAction = System.Drawing.Printing.PrintAction.PrintToPreview
        Me.imprimir.PrinterSettings = CType(resources.GetObject("imprimir.PrinterSettings"), System.Drawing.Printing.PrinterSettings)
        Me.imprimir.PrintFileName = Nothing
        '
        'ErrorProvider1
        '
        Me.ErrorProvider1.ContainerControl = Me
        '
        'Timer1
        '
        Me.Timer1.Interval = 2000
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        Me.Label5.Font = New System.Drawing.Font("Agency FB", 13.0!, System.Drawing.FontStyle.Bold)
        Me.Label5.Location = New System.Drawing.Point(438, 66)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(100, 22)
        Me.Label5.TabIndex = 46
        Me.Label5.Text = "#########"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.BackColor = System.Drawing.Color.Transparent
        Me.Label8.Font = New System.Drawing.Font("Agency FB", 13.0!, System.Drawing.FontStyle.Bold)
        Me.Label8.Location = New System.Drawing.Point(438, 97)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(100, 22)
        Me.Label8.TabIndex = 47
        Me.Label8.Text = "#########"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.BackColor = System.Drawing.Color.Transparent
        Me.Label11.Font = New System.Drawing.Font("Nirmala UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label11.Location = New System.Drawing.Point(88, 220)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(365, 21)
        Me.Label11.TabIndex = 48
        Me.Label11.Text = "Gracias por su compra que tenga un feliz viaje "
        '
        'Label12
        '
        Label12.AutoSize = True
        Label12.BackColor = System.Drawing.Color.Transparent
        Label12.ForeColor = System.Drawing.Color.Turquoise
        Label12.Location = New System.Drawing.Point(89, 26)
        Label12.Name = "Label12"
        Label12.Size = New System.Drawing.Size(44, 13)
        Label12.TabIndex = 49
        Label12.Text = "davidjc."
        '
        'BoletoFormato
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Lime
        Me.BackgroundImage = Global.SistemaDeAutomatizacion.My.Resources.Resources.BoletoLayout
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(550, 250)
        Me.Controls.Add(Label12)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Label2)
        Me.Controls.Add(Label1)
        Me.Controls.Add(Me.nombre)
        Me.Controls.Add(Me.apellido)
        Me.Controls.Add(Me.cedula)
        Me.Controls.Add(CedulaLabel)
        Me.Controls.Add(ApellidoLabel)
        Me.Controls.Add(NombreLabel)
        Me.Controls.Add(Me.fecha_actual)
        Me.Controls.Add(Label4)
        Me.Controls.Add(Label3)
        Me.Controls.Add(Me.hora_actual)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label10)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "BoletoFormato"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.TransparencyKey = System.Drawing.Color.Lime
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents nombre As Label
    Friend WithEvents apellido As Label
    Friend WithEvents cedula As Label
    Friend WithEvents fecha_actual As Label
    Friend WithEvents hora_actual As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents imprimir As PowerPacks.Printing.PrintForm
    Friend WithEvents ErrorProvider1 As ErrorProvider
    Friend WithEvents Timer1 As Timer
    Friend WithEvents Label8 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label9 As Label
End Class
