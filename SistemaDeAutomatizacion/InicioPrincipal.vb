﻿Public Class InicioPrincipal
    ' TODO: inserte el código para realizar autenticación personalizada usando el nombre de usuario y la contraseña proporcionada 
    ' (Consulte http://go.microsoft.com/fwlink/?LinkId=35339).  
    ' El objeto principal personalizado se puede adjuntar al objeto principal del subproceso actual como se indica a continuación: 
    '     My.User.CurrentPrincipal = CustomPrincipal
    ' donde CustomPrincipal es la implementación de IPrincipal utilizada para realizar la autenticación. 
    ' Posteriormente, My.User devolverá la información de identidad encapsulada en el objeto CustomPrincipal
    ' como el nombre de usuario, nombre para mostrar, etc.
    Private Sub OK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK.Click
        validar()
    End Sub
    Private Sub Cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel.Click
        Me.Close()
    End Sub
    Sub validar()
        If UsernameTextBox.Text = "" Or PasswordTextBox.Text = "" Then
            MsgBox("Porfavor ingrese sus datos para iniciar sesion! " + vbNewLine + "No debe dejar ninguncampo en blanco", MsgBoxStyle.Critical)
        ElseIf PasswordTextBox.Text = "servicio de transporte" Then
            MsgBox("Bienvenido a la Asociacion Cooperativa de transporte Union Encarnacion Barlovento Oriente R.L " + UsernameTextBox.Text + "!", MsgBoxStyle.Information)
            Me.Hide()
            MENUfrm.Show()
        Else
            MsgBox("Contraseña invalida Acceso denegado al usuario " + vbNewLine + UsernameTextBox.Text, MsgBoxStyle.Exclamation)
        End If
    End Sub
    Private Sub hora_Tick(sender As Object, e As EventArgs) Handles hora.Tick
        horalbl.Text = System.DateTime.Now
    End Sub
End Class