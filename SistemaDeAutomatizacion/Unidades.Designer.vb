﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Unidades
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim ModeloLabel As System.Windows.Forms.Label
        Dim Numero_MatriculaLabel As System.Windows.Forms.Label
        Dim NUmero_AcientosLabel As System.Windows.Forms.Label
        Dim FotoLabel As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Unidades))
        Me.TaxiDBDataConfig = New SistemaDeAutomatizacion.TaxiDBDataConfig()
        Me.UnidadesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.UnidadesTableAdapter = New SistemaDeAutomatizacion.TaxiDBDataConfigTableAdapters.UnidadesTableAdapter()
        Me.TableAdapterManager = New SistemaDeAutomatizacion.TaxiDBDataConfigTableAdapters.TableAdapterManager()
        Me.UnidadesBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.ToolStripButton5 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripLabel1 = New System.Windows.Forms.ToolStripLabel()
        Me.ToolStripButton6 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton1 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton2 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripTextBox1 = New System.Windows.Forms.ToolStripTextBox()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripButton3 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton4 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.UnidadesBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.ModeloTextBox = New System.Windows.Forms.TextBox()
        Me.Numero_MatriculaTextBox1 = New System.Windows.Forms.TextBox()
        Me.NUmero_AcientosTextBox = New System.Windows.Forms.TextBox()
        Me.FotoPictureBox = New System.Windows.Forms.PictureBox()
        ModeloLabel = New System.Windows.Forms.Label()
        Numero_MatriculaLabel = New System.Windows.Forms.Label()
        NUmero_AcientosLabel = New System.Windows.Forms.Label()
        FotoLabel = New System.Windows.Forms.Label()
        CType(Me.TaxiDBDataConfig, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UnidadesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UnidadesBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.UnidadesBindingNavigator.SuspendLayout()
        CType(Me.FotoPictureBox, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ModeloLabel
        '
        ModeloLabel.AutoSize = True
        ModeloLabel.Location = New System.Drawing.Point(19, 42)
        ModeloLabel.Name = "ModeloLabel"
        ModeloLabel.Size = New System.Drawing.Size(45, 13)
        ModeloLabel.TabIndex = 3
        ModeloLabel.Text = "Modelo:"
        '
        'Numero_MatriculaLabel
        '
        Numero_MatriculaLabel.AutoSize = True
        Numero_MatriculaLabel.Location = New System.Drawing.Point(19, 68)
        Numero_MatriculaLabel.Name = "Numero_MatriculaLabel"
        Numero_MatriculaLabel.Size = New System.Drawing.Size(93, 13)
        Numero_MatriculaLabel.TabIndex = 5
        Numero_MatriculaLabel.Text = "Numero Matricula:"
        '
        'NUmero_AcientosLabel
        '
        NUmero_AcientosLabel.AutoSize = True
        NUmero_AcientosLabel.Location = New System.Drawing.Point(19, 94)
        NUmero_AcientosLabel.Name = "NUmero_AcientosLabel"
        NUmero_AcientosLabel.Size = New System.Drawing.Size(93, 13)
        NUmero_AcientosLabel.TabIndex = 7
        NUmero_AcientosLabel.Text = "NUmero Acientos:"
        '
        'FotoLabel
        '
        FotoLabel.AutoSize = True
        FotoLabel.Location = New System.Drawing.Point(224, 28)
        FotoLabel.Name = "FotoLabel"
        FotoLabel.Size = New System.Drawing.Size(31, 13)
        FotoLabel.TabIndex = 9
        FotoLabel.Text = "Foto:"
        '
        'TaxiDBDataConfig
        '
        Me.TaxiDBDataConfig.DataSetName = "TaxiDBDataConfig"
        Me.TaxiDBDataConfig.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'UnidadesBindingSource
        '
        Me.UnidadesBindingSource.DataMember = "Unidades"
        Me.UnidadesBindingSource.DataSource = Me.TaxiDBDataConfig
        '
        'UnidadesTableAdapter
        '
        Me.UnidadesTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.BoletoTableAdapter = Nothing
        Me.TableAdapterManager.ClienteTableAdapter = Nothing
        Me.TableAdapterManager.DestinosTableAdapter = Nothing
        Me.TableAdapterManager.UnidadesTableAdapter = Me.UnidadesTableAdapter
        Me.TableAdapterManager.UpdateOrder = SistemaDeAutomatizacion.TaxiDBDataConfigTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'UnidadesBindingNavigator
        '
        Me.UnidadesBindingNavigator.AddNewItem = Me.ToolStripButton5
        Me.UnidadesBindingNavigator.BindingSource = Me.UnidadesBindingSource
        Me.UnidadesBindingNavigator.CountItem = Me.ToolStripLabel1
        Me.UnidadesBindingNavigator.DeleteItem = Me.ToolStripButton6
        Me.UnidadesBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButton1, Me.ToolStripButton2, Me.ToolStripSeparator1, Me.ToolStripTextBox1, Me.ToolStripLabel1, Me.ToolStripSeparator2, Me.ToolStripButton3, Me.ToolStripButton4, Me.ToolStripSeparator3, Me.ToolStripButton5, Me.ToolStripButton6, Me.UnidadesBindingNavigatorSaveItem})
        Me.UnidadesBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.UnidadesBindingNavigator.MoveFirstItem = Me.ToolStripButton1
        Me.UnidadesBindingNavigator.MoveLastItem = Me.ToolStripButton4
        Me.UnidadesBindingNavigator.MoveNextItem = Me.ToolStripButton3
        Me.UnidadesBindingNavigator.MovePreviousItem = Me.ToolStripButton2
        Me.UnidadesBindingNavigator.Name = "UnidadesBindingNavigator"
        Me.UnidadesBindingNavigator.PositionItem = Me.ToolStripTextBox1
        Me.UnidadesBindingNavigator.Size = New System.Drawing.Size(390, 25)
        Me.UnidadesBindingNavigator.TabIndex = 0
        Me.UnidadesBindingNavigator.Text = "BindingNavigator1"
        '
        'ToolStripButton5
        '
        Me.ToolStripButton5.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton5.Image = CType(resources.GetObject("ToolStripButton5.Image"), System.Drawing.Image)
        Me.ToolStripButton5.Name = "ToolStripButton5"
        Me.ToolStripButton5.RightToLeftAutoMirrorImage = True
        Me.ToolStripButton5.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton5.Text = "Agregar nuevo"
        '
        'ToolStripLabel1
        '
        Me.ToolStripLabel1.Name = "ToolStripLabel1"
        Me.ToolStripLabel1.Size = New System.Drawing.Size(38, 22)
        Me.ToolStripLabel1.Text = "de {0}"
        Me.ToolStripLabel1.ToolTipText = "Número total de elementos"
        '
        'ToolStripButton6
        '
        Me.ToolStripButton6.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton6.Image = CType(resources.GetObject("ToolStripButton6.Image"), System.Drawing.Image)
        Me.ToolStripButton6.Name = "ToolStripButton6"
        Me.ToolStripButton6.RightToLeftAutoMirrorImage = True
        Me.ToolStripButton6.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton6.Text = "Eliminar"
        '
        'ToolStripButton1
        '
        Me.ToolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton1.Image = CType(resources.GetObject("ToolStripButton1.Image"), System.Drawing.Image)
        Me.ToolStripButton1.Name = "ToolStripButton1"
        Me.ToolStripButton1.RightToLeftAutoMirrorImage = True
        Me.ToolStripButton1.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton1.Text = "Mover primero"
        '
        'ToolStripButton2
        '
        Me.ToolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton2.Image = CType(resources.GetObject("ToolStripButton2.Image"), System.Drawing.Image)
        Me.ToolStripButton2.Name = "ToolStripButton2"
        Me.ToolStripButton2.RightToLeftAutoMirrorImage = True
        Me.ToolStripButton2.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton2.Text = "Mover anterior"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'ToolStripTextBox1
        '
        Me.ToolStripTextBox1.AccessibleName = "Posición"
        Me.ToolStripTextBox1.AutoSize = False
        Me.ToolStripTextBox1.Name = "ToolStripTextBox1"
        Me.ToolStripTextBox1.Size = New System.Drawing.Size(50, 23)
        Me.ToolStripTextBox1.Text = "0"
        Me.ToolStripTextBox1.ToolTipText = "Posición actual"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'ToolStripButton3
        '
        Me.ToolStripButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton3.Image = CType(resources.GetObject("ToolStripButton3.Image"), System.Drawing.Image)
        Me.ToolStripButton3.Name = "ToolStripButton3"
        Me.ToolStripButton3.RightToLeftAutoMirrorImage = True
        Me.ToolStripButton3.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton3.Text = "Mover siguiente"
        '
        'ToolStripButton4
        '
        Me.ToolStripButton4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton4.Image = CType(resources.GetObject("ToolStripButton4.Image"), System.Drawing.Image)
        Me.ToolStripButton4.Name = "ToolStripButton4"
        Me.ToolStripButton4.RightToLeftAutoMirrorImage = True
        Me.ToolStripButton4.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton4.Text = "Mover último"
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(6, 25)
        '
        'UnidadesBindingNavigatorSaveItem
        '
        Me.UnidadesBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.UnidadesBindingNavigatorSaveItem.Image = CType(resources.GetObject("UnidadesBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.UnidadesBindingNavigatorSaveItem.Name = "UnidadesBindingNavigatorSaveItem"
        Me.UnidadesBindingNavigatorSaveItem.Size = New System.Drawing.Size(23, 22)
        Me.UnidadesBindingNavigatorSaveItem.Text = "Guardar datos"
        '
        'ModeloTextBox
        '
        Me.ModeloTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.UnidadesBindingSource, "Modelo", True))
        Me.ModeloTextBox.Location = New System.Drawing.Point(118, 39)
        Me.ModeloTextBox.Name = "ModeloTextBox"
        Me.ModeloTextBox.Size = New System.Drawing.Size(100, 20)
        Me.ModeloTextBox.TabIndex = 4
        '
        'Numero_MatriculaTextBox1
        '
        Me.Numero_MatriculaTextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.UnidadesBindingSource, "Numero_Matricula", True))
        Me.Numero_MatriculaTextBox1.Location = New System.Drawing.Point(118, 65)
        Me.Numero_MatriculaTextBox1.Name = "Numero_MatriculaTextBox1"
        Me.Numero_MatriculaTextBox1.Size = New System.Drawing.Size(100, 20)
        Me.Numero_MatriculaTextBox1.TabIndex = 6
        '
        'NUmero_AcientosTextBox
        '
        Me.NUmero_AcientosTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.UnidadesBindingSource, "NUmero_Acientos", True))
        Me.NUmero_AcientosTextBox.Location = New System.Drawing.Point(118, 91)
        Me.NUmero_AcientosTextBox.Name = "NUmero_AcientosTextBox"
        Me.NUmero_AcientosTextBox.Size = New System.Drawing.Size(100, 20)
        Me.NUmero_AcientosTextBox.TabIndex = 8
        '
        'FotoPictureBox
        '
        Me.FotoPictureBox.DataBindings.Add(New System.Windows.Forms.Binding("Image", Me.UnidadesBindingSource, "Foto", True))
        Me.FotoPictureBox.Location = New System.Drawing.Point(261, 28)
        Me.FotoPictureBox.Name = "FotoPictureBox"
        Me.FotoPictureBox.Size = New System.Drawing.Size(121, 98)
        Me.FotoPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.FotoPictureBox.TabIndex = 10
        Me.FotoPictureBox.TabStop = False
        '
        'Unidades
        '
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(390, 133)
        Me.Controls.Add(FotoLabel)
        Me.Controls.Add(Me.FotoPictureBox)
        Me.Controls.Add(ModeloLabel)
        Me.Controls.Add(Me.ModeloTextBox)
        Me.Controls.Add(Numero_MatriculaLabel)
        Me.Controls.Add(Me.Numero_MatriculaTextBox1)
        Me.Controls.Add(NUmero_AcientosLabel)
        Me.Controls.Add(Me.NUmero_AcientosTextBox)
        Me.Controls.Add(Me.UnidadesBindingNavigator)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "Unidades"
        Me.Text = "Unidades"
        CType(Me.TaxiDBDataConfig, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UnidadesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UnidadesBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.UnidadesBindingNavigator.ResumeLayout(False)
        Me.UnidadesBindingNavigator.PerformLayout()
        CType(Me.FotoPictureBox, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Protected WithEvents GroupBox1 As GroupBox
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents TxtBuscar As TextBox
    Friend WithEvents BUSCAR_BTN As Button
    Friend WithEvents UPDATEDB As Button
    Friend WithEvents DEL_Cliente As Button
    Friend WithEvents ADD_Cliente As Button
    Friend WithEvents GroupBox2 As GroupBox

    Friend WithEvents ConductorBindingSource As BindingSource

    Friend WithEvents ConductorBindingNavigator As BindingNavigator
    Friend WithEvents BindingNavigatorAddNewItem As ToolStripButton
    Friend WithEvents BindingNavigatorCountItem As ToolStripLabel
    Friend WithEvents BindingNavigatorDeleteItem As ToolStripButton
    Friend WithEvents BindingNavigatorMoveFirstItem As ToolStripButton
    Friend WithEvents BindingNavigatorMovePreviousItem As ToolStripButton
    Friend WithEvents BindingNavigatorSeparator As ToolStripSeparator
    Friend WithEvents BindingNavigatorPositionItem As ToolStripTextBox
    Friend WithEvents BindingNavigatorSeparator1 As ToolStripSeparator
    Friend WithEvents BindingNavigatorMoveNextItem As ToolStripButton
    Friend WithEvents BindingNavigatorMoveLastItem As ToolStripButton
    Friend WithEvents BindingNavigatorSeparator2 As ToolStripSeparator
    Friend WithEvents ConductorBindingNavigatorSaveItem As ToolStripButton
    Friend WithEvents PuestosDisponiblesTextBox As TextBox
    Friend WithEvents Numero_MatriculaTextBox As TextBox
    Friend WithEvents ApellidoTextBox As TextBox
    Friend WithEvents CedulaTextBox As TextBox
    Friend WithEvents NombreTextBox As TextBox
    Friend WithEvents BoletoBindingSource As BindingSource

    Friend WithEvents COD_DestinoComboBox As ComboBox
    Friend WithEvents TaxiDBDataConfig As TaxiDBDataConfig
    Friend WithEvents UnidadesBindingSource As BindingSource
    Friend WithEvents UnidadesTableAdapter As TaxiDBDataConfigTableAdapters.UnidadesTableAdapter
    Friend WithEvents TableAdapterManager As TaxiDBDataConfigTableAdapters.TableAdapterManager
    Friend WithEvents UnidadesBindingNavigator As BindingNavigator
    Friend WithEvents ToolStripButton5 As ToolStripButton
    Friend WithEvents ToolStripLabel1 As ToolStripLabel
    Friend WithEvents ToolStripButton6 As ToolStripButton
    Friend WithEvents ToolStripButton1 As ToolStripButton
    Friend WithEvents ToolStripButton2 As ToolStripButton
    Friend WithEvents ToolStripSeparator1 As ToolStripSeparator
    Friend WithEvents ToolStripTextBox1 As ToolStripTextBox
    Friend WithEvents ToolStripSeparator2 As ToolStripSeparator
    Friend WithEvents ToolStripButton3 As ToolStripButton
    Friend WithEvents ToolStripButton4 As ToolStripButton
    Friend WithEvents ToolStripSeparator3 As ToolStripSeparator
    Friend WithEvents UnidadesBindingNavigatorSaveItem As ToolStripButton
    Friend WithEvents ModeloTextBox As TextBox
    Friend WithEvents Numero_MatriculaTextBox1 As TextBox
    Friend WithEvents NUmero_AcientosTextBox As TextBox
    Friend WithEvents FotoPictureBox As PictureBox
End Class
