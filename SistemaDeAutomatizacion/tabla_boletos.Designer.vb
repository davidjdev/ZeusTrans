﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class tabla_ventas
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.TaxiDBDataConfig = New SistemaDeAutomatizacion.TaxiDBDataConfig()
        Me.Boletos_VendidosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Boletos_VendidosTableAdapter = New SistemaDeAutomatizacion.TaxiDBDataConfigTableAdapters.Boletos_VendidosTableAdapter()
        Me.TableAdapterManager = New SistemaDeAutomatizacion.TaxiDBDataConfigTableAdapters.TableAdapterManager()
        Me.Boletos_VendidosDataGridView = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.TaxiDBDataConfig, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Boletos_VendidosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Boletos_VendidosDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TaxiDBDataConfig
        '
        Me.TaxiDBDataConfig.DataSetName = "TaxiDBDataConfig"
        Me.TaxiDBDataConfig.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Boletos_VendidosBindingSource
        '
        Me.Boletos_VendidosBindingSource.DataMember = "Boletos_Vendidos"
        Me.Boletos_VendidosBindingSource.DataSource = Me.TaxiDBDataConfig
        '
        'Boletos_VendidosTableAdapter
        '
        Me.Boletos_VendidosTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.BoletoTableAdapter = Nothing
        Me.TableAdapterManager.ClienteTableAdapter = Nothing
        Me.TableAdapterManager.Connection = Nothing
        Me.TableAdapterManager.DestinosTableAdapter = Nothing
        Me.TableAdapterManager.UnidadesTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = SistemaDeAutomatizacion.TaxiDBDataConfigTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'Boletos_VendidosDataGridView
        '
        Me.Boletos_VendidosDataGridView.AutoGenerateColumns = False
        Me.Boletos_VendidosDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.Boletos_VendidosDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn4, Me.DataGridViewTextBoxColumn5, Me.DataGridViewTextBoxColumn6, Me.DataGridViewTextBoxColumn7, Me.DataGridViewTextBoxColumn8, Me.DataGridViewTextBoxColumn9})
        Me.Boletos_VendidosDataGridView.DataSource = Me.Boletos_VendidosBindingSource
        Me.Boletos_VendidosDataGridView.Location = New System.Drawing.Point(-2, -1)
        Me.Boletos_VendidosDataGridView.Name = "Boletos_VendidosDataGridView"
        Me.Boletos_VendidosDataGridView.Size = New System.Drawing.Size(300, 220)
        Me.Boletos_VendidosDataGridView.TabIndex = 1
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "Cedula"
        Me.DataGridViewTextBoxColumn1.HeaderText = "Cedula"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "Apellido"
        Me.DataGridViewTextBoxColumn2.HeaderText = "Apellido"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "Nombre"
        Me.DataGridViewTextBoxColumn3.HeaderText = "Nombre"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "Telefono"
        Me.DataGridViewTextBoxColumn4.HeaderText = "Telefono"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.DataPropertyName = "Cod_Boleto"
        Me.DataGridViewTextBoxColumn5.HeaderText = "Cod_Boleto"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.DataPropertyName = "Fecha_venta"
        Me.DataGridViewTextBoxColumn6.HeaderText = "Fecha_venta"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.DataPropertyName = "Destino"
        Me.DataGridViewTextBoxColumn7.HeaderText = "Destino"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.DataPropertyName = "Direccion"
        Me.DataGridViewTextBoxColumn8.HeaderText = "Direccion"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.DataPropertyName = "Valor"
        Me.DataGridViewTextBoxColumn9.HeaderText = "Valor"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        '
        'tabla_ventas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(815, 348)
        Me.Controls.Add(Me.Boletos_VendidosDataGridView)
        Me.Name = "tabla_ventas"
        Me.Text = "CONTROL DE VENTAS"
        CType(Me.TaxiDBDataConfig, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Boletos_VendidosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Boletos_VendidosDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents TaxiDBDataConfig As TaxiDBDataConfig
    Friend WithEvents Boletos_VendidosBindingSource As BindingSource
    Friend WithEvents Boletos_VendidosTableAdapter As TaxiDBDataConfigTableAdapters.Boletos_VendidosTableAdapter
    Friend WithEvents TableAdapterManager As TaxiDBDataConfigTableAdapters.TableAdapterManager
    Friend WithEvents Boletos_VendidosDataGridView As DataGridView
    Friend WithEvents DataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As DataGridViewTextBoxColumn
End Class
