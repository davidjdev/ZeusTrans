﻿Public Class BoletoFormato
    Dim nombre_cliente As String
    Dim apellido_cliente As String
    Dim cedula_cliente As String
    Dim hora_venta As String
    Dim fecha_venta As String
    Dim dir_oficina As String
    Dim dir_destino As String
    Dim precio As String
    Dim cod_unidad As String
    Dim cod_matricula As String
    Sub New(ByVal nombre_cliente As String, ByVal apellido_cliente As String, ByVal cedula_cliente As String,
            ByVal hora_venta As String, ByVal fecha_venta As String, ByVal dir_oficina As String,
            ByVal dir_destino As String, ByVal precio As String, ByVal cod_unidad As String,
            ByVal cod_matricula As String)
        'En este sub lo que hago es capturar los parametros que paso desde el formulario de clientes 
        InitializeComponent() 'es necesario que lleve esta linea para que inicialice los componentes y las variables dentro del form  
        cedula.Text = cedula_cliente
        apellido.Text = apellido_cliente
        nombre.Text = nombre_cliente
        hora_actual.Text = hora_venta
        fecha_actual.Text = fecha_venta
        Label7.Text = dir_oficina
        Label6.Text = dir_destino
        Label9.Text = precio & ",00"
        Label5.Text = cod_unidad
        Label8.Text = cod_matricula
        'telefono.Text = telefono_cliente
        'lbl_codcli.Text = codigo_cliente
    End Sub
    Private Sub BoletoFormato_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Timer1.Start()
    End Sub

    Private Sub BoletoFormato_MouseDoubleClick(sender As Object, e As MouseEventArgs) Handles Me.MouseDoubleClick
        Me.Close()
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        On Error Resume Next
        imprimir.Print()
        Timer1.Stop()
    End Sub
End Class