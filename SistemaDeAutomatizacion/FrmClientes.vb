﻿Imports System.ComponentModel

Public Class FrmClientes

    Private Sub FrmClientes_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        'TODO: esta línea de código carga datos en la tabla 'TaxiDBDataSet.Cliente' Puede moverla o quitarla según sea necesario.
        Cargar_Estilo()
        Me.TableAdapterManager.ClienteTableAdapter.Fill(TaxiDBDataConfig.Cliente)

    End Sub

    Private Sub ToolStripBuscarTxt_KeyPress(sender As Object, e As KeyPressEventArgs)
        ''  If Not (Char.IsNumber(e.KeyChar) Or e.KeyChar.Equals(Sep) Or Char.IsControl(e.KeyChar)) Then e.Handled = True
    End Sub



    Private Sub DEL_Cliente_Click(sender As Object, e As EventArgs) Handles DEL_Cliente.Click

        If MsgBox("Eliminar Cliente?", vbQuestion + MsgBoxStyle.YesNo, "Confirmacion") = vbYes Then
            ClienteBindingSource.RemoveCurrent()
            ClienteBindingSource.Filter = Nothing
            MsgBox("Operacion Completada!", vbInformation, "Cliente Eliminado!")
        Else
        End If

    End Sub

    Private Sub UPDATEDB_Click(sender As Object, e As EventArgs) Handles UPDATEDB.Click
        ADD_Cliente.Enabled = True
        BUSCAR_BTN.Enabled = True
        DEL_Cliente.Enabled = True
        TxtBuscar.Select()
        On Error GoTo ErrGuardar

        Me.ClienteBindingSource.Filter = Nothing
        If MsgBox("Guardar Cambios?", vbQuestion + MsgBoxStyle.YesNo, "Confirmacion") = vbYes Then
            If Me.Validate = True Then
                ClienteBindingSource.EndEdit()
                ClienteTableAdapter.Update(TaxiDBDataConfig.Cliente)
                Me.TableAdapterManager.ClienteTableAdapter.Fill(TaxiDBDataConfig.Cliente)
            Else
                System.Windows.Forms.MessageBox.Show(Me, "Error De Validacion.", "Fallo", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning)

            End If
        Else

            Me.TableAdapterManager.ClienteTableAdapter.Fill(TaxiDBDataConfig.Cliente)
        End If


        MsgBox("Operacion Completada!", vbInformation, "Cliente Eliminado!")


ErrEx:
        Exit Sub
ErrGuardar:
        If Err.Number = 5 Then
            MsgBox("Ya existe un registro con ese numero de Cedula", vbExclamation, "Fail")
            ClienteBindingSource.CancelEdit()

            Cargar_Estilo()
            Me.TableAdapterManager.ClienteTableAdapter.Fill(TaxiDBDataConfig.Cliente)


        Else
            MsgBox("Error Numero " & Err.Number & vbNewLine &
               "Error Descripcion " & Err.Description, MsgBoxStyle.Critical,
               "Reset Error!")
            Resume ErrEx
        End If

    End Sub

    Private Sub ADD_Cliente_Click(sender As Object, e As EventArgs) Handles ADD_Cliente.Click
        ADD_Cliente.Enabled = False
        BUSCAR_BTN.Enabled = False
        DEL_Cliente.Enabled = False
        CedulaTextBox.Select()
        Me.ClienteBindingSource.Filter = Nothing
        ClienteBindingSource.AddNew()

    End Sub

    Private Sub BUSCAR_BTN_Click(sender As Object, e As EventArgs) Handles BUSCAR_BTN.Click
        On Error GoTo ErrBuscar
        If TxtBuscar.Text = "" Or TxtBuscar.Text = "CEDULA" Then
            MsgBox("Debe colocar la cedula del cliente a buscar", vbExclamation, "Info")
            Me.TableAdapterManager.ClienteTableAdapter.Fill(TaxiDBDataConfig.Cliente)
            Exit Sub
        Else

            ClienteBindingSource.Filter = String.Format("Convert (Cedula,'System.String') like '%" & TxtBuscar.Text & "%'")
            If ClienteBindingSource.Count <> 0 Then
                ClienteDataGridView.DataSource = ClienteBindingSource
            Else
                MsgBox("No existe el cliente en la base de datos!", vbExclamation, "OJo")
                Me.ClienteBindingSource.Filter = Nothing
                Me.TableAdapterManager.ClienteTableAdapter.Fill(TaxiDBDataConfig.Cliente)
            End If
        End If
ErrBuscar:
        Me.TableAdapterManager.ClienteTableAdapter.Fill(TaxiDBDataConfig.Cliente)
ErrEx:
        Exit Sub
ErrRe:
        MsgBox("Error Numero " & Err.Number & vbNewLine &
               "Error Descripcion " & Err.Description, MsgBoxStyle.Critical,
               "Reset Error!")
    End Sub

    Private Sub TxtBuscar_KeyDown(sender As Object, e As KeyEventArgs) Handles TxtBuscar.KeyDown
        If e.KeyCode = Keys.Enter Then
            BUSCAR_BTN.PerformClick()
        End If
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs)
        ClienteBindingSource.Filter = Nothing
    End Sub
    Sub Cargar_Estilo()
        With ClienteDataGridView
            .RowsDefaultCellStyle.BackColor = Color.Aquamarine
            .AlternatingRowsDefaultCellStyle.BackColor = Color.LightCyan
        End With
    End Sub

    Private Sub VENDER_BTN_Click(sender As Object, e As EventArgs) Handles VENDER_BTN.Click

        Try
            VENDER_BTN.Enabled = False
            Dim ventas As New frmVentas(NombreTextBox.Text, ApellidoTextBox.Text, CedulaTextBox.Text, TelefonoTextBox.Text, cod_cliente.Text)
            ventas.ShowDialog(Me)
        Catch ex As Exception
            MsgBox("Error Contacte con el programador!")
        Finally
            VENDER_BTN.Enabled = True

        End Try
    End Sub

End Class